//
//  PhotosPresenter.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/29/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public class PhotosPresenter: PhotosPresenterDelegate {
    
    weak var view: PhotosViewController?
    
    func getUserPhotos(limit: String, completion: @escaping (RandomUserResponse?, Error?) -> Void) {
        let networking = ZenlyNetworkProvider<RandomUserAPI>(networkClient: URLSessionNetworkClient(session: .shared))
        let repo = RandomUserRepository(networking: networking)
        
        repo.getUsers(limit: "100") { randomUserData, error in
            completion(randomUserData, error)
        }
    }
}
