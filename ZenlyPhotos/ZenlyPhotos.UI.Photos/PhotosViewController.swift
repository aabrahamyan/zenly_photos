//
//  ViewController.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/25/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import UIKit

protocol PhotosPresenterDelegate {
    func getUserPhotos(limit: String, completion: @escaping (RandomUserResponse?, Error?) -> Void)
}

class PhotosViewController: UIViewController {
    struct Constants {
        static let cellidentifier = "com.zenly.photocell"
    }
    @IBOutlet weak var collectionView: UICollectionView!
    
    typealias ViewState = RandomUserResponse
    var viewState: ViewState? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var presenter: PhotosPresenter?
    var shareBarButtonItem: UIBarButtonItem = {
        return UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
    }()
    var gridLayout = GridFlowLayout()
    private let imageLoadingQueue = OperationQueue()
    private var imageLoadOperations = [IndexPath: ImageLoadingOperation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = shareBarButtonItem
        
        self.collectionView.collectionViewLayout = gridLayout        
        self.collectionView.prefetchDataSource = self
        
        self.presenter = PhotosPresenter()
        self.presenter?.view = self
        
        // Load Photos
        self.presenter?.getUserPhotos(limit: "100", completion: {[weak self] randomUser, error in
            guard error == nil else { return }
            DispatchQueue.main.async {
                self?.viewState = randomUser
            }
        })
    }
}

// MARK: Actions
extension PhotosViewController {
    
    @objc private func shareTapped() {
        
    }
}

// MARK: DataSource and Delegate (Delegate Missing Now)
extension PhotosViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewState = viewState else { return 0 }
        return viewState.results.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellidentifier, for: indexPath) as! PhotoCollectionViewCell
        if let viewState = self.viewState?.results[indexPath.row] {
            //cell.updateCellA(viewState) // Used for Variant A
            if let imageLoadingOperation = self.imageLoadOperations[indexPath], let image = imageLoadingOperation.image {
                cell.updateCellB(image: image)
            } else {
                let imageLoadingOperation = ImageLoadingOperation(url: viewState.picture.large)
                imageLoadingOperation.completionHandler = { [weak self] image in
                    guard let strongSelf = self else { return }
                                        
                    strongSelf.imageLoadOperations.removeValue(forKey: indexPath)
                    
                    DispatchQueue.main.async {
                        cell.updateCellB(image: image)
                    }
                }
                
                imageLoadingQueue.addOperation(imageLoadingOperation)
                imageLoadOperations[indexPath] = imageLoadingOperation
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let imageLoadingOperation = imageLoadOperations[indexPath] else { return }
        imageLoadingOperation.cancel()
        imageLoadOperations.removeValue(forKey: indexPath)
    }
}

// MARK: Prefatching Images
extension PhotosViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let _ = imageLoadOperations[indexPath] {
            
            }
            
            if let cellViewState = viewState?.results[indexPath.row] {
                let imageLoadingOperation = ImageLoadingOperation(url: cellViewState.picture.large)
                self.imageLoadingQueue.addOperation(imageLoadingOperation)
                self.imageLoadOperations[indexPath] = imageLoadingOperation
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            guard let imageLoadingOperation = self.imageLoadOperations[indexPath] else { return }
            
            imageLoadingOperation.cancel()
            self.imageLoadOperations.removeValue(forKey: indexPath)                        
        }
    }
}

