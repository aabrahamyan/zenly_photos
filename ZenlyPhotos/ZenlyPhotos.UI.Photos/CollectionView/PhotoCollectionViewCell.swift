//
//  PhotoCollectionViewCell.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/29/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    typealias ViewState = UserDataEntity
    public var viewState: ViewState? {
        didSet {
            // Variant A:
            //self.updateCellA()
            
            // Variant B:
            
        }
    }
    
// MARK: Variant A: Load with GCD using dataTask
    public func updateCellA() {
        guard let urlString = viewState?.picture.large else { return }
        imageView.downloadImageFromUrl(urlString)
    }

// MARK: Variant B: Load with Operation using collection view prefetching
    public func updateCellB(image: UIImage) {
        imageView.image = image
    }
}
