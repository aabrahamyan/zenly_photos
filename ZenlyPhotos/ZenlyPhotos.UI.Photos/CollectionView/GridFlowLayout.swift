//
//  GridFlowLayout.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/29/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import UIKit

final class GridFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        self.minimumLineSpacing = 2
        self.minimumInteritemSpacing = 2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureLayout() {
        self.scrollDirection = .vertical
        if let collectionView = self.collectionView {
            let optimisedWidth = (collectionView.frame.width - minimumInteritemSpacing) / 2
            self.itemSize = CGSize(width: optimisedWidth , height: optimisedWidth)
        }
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.configureLayout()
    }
}
