//
//  ImageLoadingOperation.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/29/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import UIKit

typealias ImageLoadingOperationCompletionHandlerType = ((UIImage) -> ())

class ImageLoadingOperation: Operation {
    var url: String
    var completionHandler: ImageLoadingOperationCompletionHandlerType?
    var image: UIImage?

    init(url: String) {
        self.url = url
    }

    override func main() {
        if isCancelled {
            return
        }

        UIImage.downloadImageFromUrl(url) { [weak self] (image) in
            guard let strongSelf = self,
                !strongSelf.isCancelled,
                let image = image else {
                return
            }
            strongSelf.image = image
            strongSelf.completionHandler?(image)
        }
    }
}
