//
//  RandomUserRepositoryProtocol.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/28/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public protocol RandomUserRepositoryProtocol {
    func getUsers(limit: String, handler: @escaping (RandomUserResponse?, Error?) -> Void)
}
