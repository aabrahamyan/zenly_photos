//
//  RandomUserRepository.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/28/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

final class RandomUserRepository: RandomUserRepositoryProtocol {
    
    let networking: ZenlyNetworkProvider<RandomUserAPI>
    
    init(networking: ZenlyNetworkProvider<RandomUserAPI>) {
        self.networking = networking
    }
    
    func getUsers(limit: String, handler: @escaping (RandomUserResponse?, Error?) -> Void) {
        self.networking.run(.getUsers(limit)) { (userData: RandomUserResponse?, error) in
            handler(userData, error)
        }
    }
}
