//
//  RandomUserAPI.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/28/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

enum RandomUserAPI: Equatable {
    case getUsers(String)
}

extension RandomUserAPI: ZenlyEndpointDescription {
        
    var baseURL: URL {
        return URL(string: "https://randomuser.me")!
    }
    
    var path: String {
        return "/api"
    }
    
    var method: HTTPMethod {
        switch self {
        case .getUsers: return .get
        }
    }
    
    var parameters: [String : String]? {
        switch self {
        case .getUsers(let limit): return ["results": limit]
        }
    }
    
    var body: Encodable? {
        return nil
    }
    
    var headers: [String : String]? {
        return ["zenly-test-header": "test"]
    }
}
