//
//  UserDataEntity.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/28/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public struct UserDataEntity: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case picture
    }
    
    public var picture: Picture
        
}

public extension UserDataEntity {
    struct Picture: Codable, Equatable {
        enum CodingKeys: String, CodingKey {
            case large
            case medium
            case thumbnail
        }
        
        public var large: String
        public var medium: String
        public var thumbnail: String
        
        public init(large: String,
                    medium: String,
                    thumbnail: String) {
            self.large = large
            self.medium = medium
            self.thumbnail = thumbnail
        }
    }
}
