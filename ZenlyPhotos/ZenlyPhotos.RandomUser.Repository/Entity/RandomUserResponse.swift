//
//  RandomUserResponse.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/28/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public struct RandomUserResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case results
    }
    
    public var results:[UserDataEntity]
    
}
