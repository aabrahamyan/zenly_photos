//
//  NetworkClient.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/25/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public enum ZenlyNetworkError: Error {
    case networkError(Error)
    case noResponse
    case unexpectedStatusCode(Int)
    case conversionFailed(Error, statusCode: Int?)
}

public enum ZenlyConversionFailedError: Error {
    case emptyData
    case stringConversionFailed
}

extension ZenlyNetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .networkError(let networkError):
            return "A Network Error: \(networkError.localizedDescription)"
        case .noResponse:
            return "No response from server."
        case .unexpectedStatusCode(let statusCode):
            return "Unexpected status code: \(statusCode)"
        case .conversionFailed(let conversionError, let statusCode):
            var result = "Conversion failed: \(conversionError.localizedDescription)"
            if let statusCode = statusCode {
                result.append(", response status code: \(statusCode)")
            }
            return result
        }
    }
}

public struct DefaultZenlyResponse {
    public let response: HTTPURLResponse?
    public let data: Data?
    public let error: ZenlyNetworkError?
    
    var statusCode: Int? {
        return response?.statusCode
    }
    
    init(data: Data?,
         response: URLResponse?,
         error: ZenlyNetworkError?) {
        
        self.data = data
        self.response = response as? HTTPURLResponse //NOTE: Because we operate with URLSessionTask
        self.error = error
    }
}

public struct ZenlyNetworkResponse<Value> {
    public let response: HTTPURLResponse?
    public let data: Data?
    public let result: Result<Value, ZenlyNetworkError>
    
    var statusCode: Int? {
        return response?.statusCode
    }
    
    init(data: Data?, response: URLResponse?, result: Result<Value, ZenlyNetworkError>) {
        self.data = data
        self.response = response as? HTTPURLResponse
        self.result = result
    }
    
    init<Default>(defaultResponse: ZenlyNetworkResponse<Default>, result: Result<Value, ZenlyNetworkError>) {
        self.data = defaultResponse.data
        self.response = defaultResponse.response
        self.result = result
    }
}
