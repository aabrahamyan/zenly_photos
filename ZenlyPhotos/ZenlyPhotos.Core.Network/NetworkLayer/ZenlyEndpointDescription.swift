//
//  ZenlyEndpointDescription.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/27/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public protocol ZenlyEndpointDescription {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: String]? { get }
    var body: Encodable? { get }
    var headers: [String: String]? { get }
}

public enum HTTPMethod: String {
    case post = "POST"
    case put = "PUT"
    case get = "GET"
    case delete = "DELETE"
}
