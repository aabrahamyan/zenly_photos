//
//  ZenlyNetworkProvider.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/27/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

/* When calling generic functions (like ZenlyNetworkClient.send<P: Encodable>(request: _, payload: P, _)
 we need to pass exact types that conform to the generic requirements. For our example, we would need
 to send an object conforming to `Encodable` as the payload. If we try to pass a non-exact Encodable object, we will get this error:
 "Protocol type Encodable cannot conform to ‘Encodable’ because only concrete types can conform to protocols".
 By creating this WrappedEncodable box that wraps a non-exact Encodable object, we can bypass the limitations.
 */
struct WrappedEncodable: Encodable {
    let wrapped: Encodable
  
    init(_ wrapped: Encodable) {
        self.wrapped = wrapped
    }
    
    func encode(to encoder: Encoder) throws {
        try wrapped.encode(to: encoder)
    }
}

public class ZenlyNetworkProvider<E: ZenlyEndpointDescription> {
    private let decoder: JSONDecoder
    private let networkClient: ZenlyNetworkClient
    
    init(overridingBaseURL baseURLOverride: URL? = nil, decoder: JSONDecoder = .init(), networkClient: ZenlyNetworkClient) {
        self.decoder = decoder
        self.networkClient = URLSessionNetworkClient(session: .shared)
    }
    
    public func run<T: Decodable>(_ endpoint: E, completion: ((T?, Swift.Error?) -> Void)?) {
        do {
            let request = try makeRequest(for: endpoint)
            if let payload = endpoint.body.map({ WrappedEncodable($0) }) {
                networkClient.send(request: request, payload: payload, decoder: decoder) { (response: ZenlyNetworkResponse<T>) in
                    self.handle(response: response, completion: completion)
                }
            } else {
                networkClient.send(request: request, decoder: decoder) { (response: ZenlyNetworkResponse<T>) in
                    self.handle(response: response, completion: completion)
                }
            }
        } catch {
            completion?(nil, error)
        }
    }
    
    public func run<T: Decodable>(_ endpoint: E, completion: ((Result<T?, Swift.Error>) -> Void)?) {
        do {
            let request = try makeRequest(for: endpoint)
            if let payload = endpoint.body.map({ WrappedEncodable($0) }) {
                networkClient.send(request: request, payload: payload, decoder: decoder) { (response: ZenlyNetworkResponse<T>) in
                    self.handle(response: response, completion: completion)
                }
            } else {
                networkClient.send(request: request, decoder: decoder) { (response: ZenlyNetworkResponse<T>) in
                    self.handle(response: response, completion: completion)
                }
            }
        } catch {
            completion?(.failure(error))
        }
    }
    
    private func handle<T>(response: ZenlyNetworkResponse<T>, completion: ((T?, Swift.Error?) -> Void)?) {
        switch response.result {
        case .success(let result):
            completion?(result, nil)
        case .failure(let error):
            completion?(nil, error)
        }
    }
    
    private func handle<T>(response: ZenlyNetworkResponse<T>, completion: ((Swift.Result<T?, Swift.Error>) -> Void)?) {
        switch response.result {
        case .success(let result):
            completion?(.success(result))
        case .failure(let error):
            completion?(.failure(error))
        }
    }
}

// MARK: Internal Helpers

extension ZenlyNetworkProvider {
    enum Error: Swift.Error {
        case failedBuildingUrlComponents(url: URL)
        case failedBuildingURL(url: URL, parameters: [String: String]?)
    }
}

extension ZenlyNetworkProvider {
    
    private func makeURL(for endpoint: E) throws -> URL {
        let partialURL = endpoint.baseURL.appendingPathComponent(endpoint.path)
        guard var components = URLComponents(url: partialURL, resolvingAgainstBaseURL: false) else {
            throw Error.failedBuildingUrlComponents(url: partialURL)
        }
        
        if let parameters = endpoint.parameters {
            var queryItems = [URLQueryItem]()
            parameters.forEach { queryItems.append(URLQueryItem(name: $0.key, value: $0.value))}
            components.queryItems = queryItems
        }
        
        guard let url = components.url else {
            throw Error.failedBuildingURL(url: partialURL, parameters: endpoint.parameters)
        }
        
        return url
    }
    
    private func makeRequest(for endpoint: E) throws -> ZenlyNetworkRequest {
        let url = try makeURL(for: endpoint)
        
        let request: ZenlyNetworkRequest = {
            var requestObject = URLRequest(url: url)
            requestObject.httpMethod = endpoint.method.rawValue
            requestObject.allHTTPHeaderFields = endpoint.headers
            return ZenlyNetworkRequest(urlRequest: requestObject)
        }()
        
        return request
    }
}
