//
//  ZenlyNetworkClient.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/26/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public protocol ZenlyNetworkClient {
    
    @discardableResult
    func send<Payload: Encodable, Result: Decodable>(request: ZenlyNetworkRequest, payload: Payload, decoder: JSONDecoder, _ completionHanlder:
    @escaping (ZenlyNetworkResponse<Result>) -> Void) -> URLSessionDataTask?
    
    @discardableResult
    func send<Result: Decodable>(request: ZenlyNetworkRequest, decoder: JSONDecoder, _ completionHandler: @escaping
        (ZenlyNetworkResponse<Result>) -> Void) -> URLSessionDataTask
    
    @discardableResult
    func send(request: ZenlyNetworkRequest, _ completionHandler: @escaping (ZenlyNetworkResponse<Data>) -> Void) -> URLSessionDataTask
}

public final class URLSessionNetworkClient: ZenlyNetworkClient {
    
    private let session: URLSession
    
    public init(session: URLSession?) {
        if let session = session {
            self.session = session
        } else {
            self.session = URLSession(configuration: .default)
        }
    }
    
    @discardableResult
    public func send<Payload, Result>(request: ZenlyNetworkRequest, payload: Payload, decoder: JSONDecoder, _ completionHanlder: @escaping (ZenlyNetworkResponse<Result>) -> Void) -> URLSessionDataTask? where Payload : Encodable, Result : Decodable {
        var requestWthPayload: ZenlyNetworkRequest
        
        do {
            requestWthPayload = try request.add(payload)
        } catch {
            let response = ZenlyNetworkResponse<Result>(data: nil, response: nil, result: .failure(.conversionFailed(error, statusCode: nil)))
            completionHanlder(response)
            return nil
        }
        
        return self.send(request: requestWthPayload, decoder: decoder, completionHanlder)
    }
    
    public func send<Result>(request: ZenlyNetworkRequest, decoder: JSONDecoder, _ completionHandler: @escaping (ZenlyNetworkResponse<Result>) -> Void) -> URLSessionDataTask where Result : Decodable {
        
        return self.send(request: request) { (zenlyResponse: ZenlyNetworkResponse<Data>) in
            
            switch zenlyResponse.result {
            case .failure(let error):
                let response = ZenlyNetworkResponse<Result>(defaultResponse: zenlyResponse, result: .failure(error))
                completionHandler(response)
            case .success(let data):
                let decodedJson: Result
                do {
                    decodedJson = try decoder.decode(Result.self, from: data)
                } catch {
                    print(error)
                    let response = ZenlyNetworkResponse<Result>(defaultResponse: zenlyResponse, result: .failure(.conversionFailed(error, statusCode: zenlyResponse.statusCode)))
                    completionHandler(response)
                    return
                }
                
                let response = ZenlyNetworkResponse<Result>(defaultResponse: zenlyResponse, result: .success(decodedJson))
                completionHandler(response)
            }
        }
    }
    
    public func send(request: ZenlyNetworkRequest, _ completionHandler: @escaping (ZenlyNetworkResponse<Data>) -> Void) -> URLSessionDataTask {
        
        return self.send(request: request) { (zenlyResponse: DefaultZenlyResponse) in
            if let error = zenlyResponse.error {
                let zenlyResponse = ZenlyNetworkResponse<Data>(data: nil,
                                                               response: zenlyResponse.response,
                                                               result: .failure(error))
                completionHandler(zenlyResponse)
                return
            }
            
            guard let data = zenlyResponse.data, data.count > 0 else {
                let zenlyResponse = ZenlyNetworkResponse<Data>(data: nil, response: zenlyResponse.response, result: .failure(.conversionFailed(ZenlyConversionFailedError.emptyData, statusCode: zenlyResponse.statusCode)))
                completionHandler(zenlyResponse)
                return
            }
            
            let zenlyResponse = ZenlyNetworkResponse(data: data, response: zenlyResponse.response, result: .success(data))
            completionHandler(zenlyResponse)
        }
    }
    
    // MARK: Private section
    
    private final func send(request: ZenlyNetworkRequest, _ completionHandler: @escaping (DefaultZenlyResponse) -> Void) -> URLSessionDataTask {
        let dataTask = session.dataTask(with: request.urlRequest) { data, response, error in
            if let error = error {
                let zenlyResponse = DefaultZenlyResponse(data: data, response: response, error: ZenlyNetworkError.networkError(error))
                completionHandler(zenlyResponse)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                let zenlyResponse = DefaultZenlyResponse(data: nil, response: nil, error: ZenlyNetworkError.noResponse)
                completionHandler(zenlyResponse)
                return
            }
            
            // NOTE: Usually positive or correct responses are between these ranges, in some custom cases much higher, but I skip it here
            guard (200..<205).contains(response.statusCode) else {
                let zenlyResponse = DefaultZenlyResponse(data: nil, response: response, error: ZenlyNetworkError.unexpectedStatusCode(response.statusCode))
                completionHandler(zenlyResponse)
                return
            }
            
            let zenlyResponse = DefaultZenlyResponse(data: data, response: response, error: nil)
            completionHandler(zenlyResponse)
        }
        
        dataTask.resume()
        return dataTask
    }
}

