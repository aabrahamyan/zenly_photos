//
//  ZenlyNetworkRequest.swift
//  ZenlyPhotos
//
//  Created by Armen Abrahamyan on 10/25/20.
//  Copyright © 2020 Armen Abrahamyan. All rights reserved.
//

import Foundation

public struct ZenlyNetworkRequest: Equatable {
    public let urlRequest: URLRequest
    
    public init(urlRequest: URLRequest) {
        self.urlRequest = urlRequest
    }
    
    public static func getRequest(url: URL,
                                  queryItems: [URLQueryItem]? = nil,
                                  httpHeaders: [String: String]? = nil) -> ZenlyNetworkRequest {
        let urlRequest = getUrlRequest(url: url, queryItems: queryItems, httpHeaders: httpHeaders)
        return ZenlyNetworkRequest(urlRequest: urlRequest)
    }
    
    // NOTE: In case we will need to send POST requests (not used in this example as we only hit images)
    public func addPayload(_ payload: Data) -> ZenlyNetworkRequest {
        var requestWithPayload = self.urlRequest
        requestWithPayload.httpBody = payload
        return ZenlyNetworkRequest(urlRequest: requestWithPayload)
    }
    
    public func add<Payload: Encodable>(_ payload: Payload) throws -> ZenlyNetworkRequest {
        let encodedPayload = try JSONEncoder().encode(payload)
        return addPayload(encodedPayload)
    }
}

private extension ZenlyNetworkRequest {
    
    private static func getUrlRequest(url: URL,
                                   queryItems: [URLQueryItem]? = nil,
                                   httpHeaders: [String: String]? = nil) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        if let queryItems = queryItems {
            components?.queryItems = queryItems
        }
        
        if let httpHeaders = httpHeaders {
            urlRequest.allHTTPHeaderFields = httpHeaders
        }
        
        urlRequest.url = components?.url
        
        return urlRequest
    }
}
