# README #

This repository represents work I've done in terms of Zenly Quizz.

## Architercture Design Components ##
* Networking (IMPLEMENTED)
* Repository (IMPLEMENTED)
* UI Architecture. (IMPLEMENTED)
* Cover logic with Unit Test (TBD, I might not make it in time, but will try to add some portions for visibility)
* Design (DONE)

### Networking ###
* Networking consists of two major components - `ZenlyNetworkProvider` and `ZenlyNetworkClient`. 
 - `ZenlyNetworkClient` is responsible for constructing request and payload (if applicable), sending request and decoding the response, the way that it can directly map response `structs` without an additional parsing.
 - `ZenlyNetworkProvider` is ageneric provider which genericaly exposes a custom endpoint `ZenlyEndpointDescription`. This gives an opportunity to reuse `ZenlyNetworkProvider` for building various client APIs without a need to develop new separate use cases or request methods. The only thing that has to be done is to describe an API with endpoints methods parameters and everything required for CRUD. 
 - Worth to mention, that this Networking layer is completly reusable and is kind of separate module. This could be moved to the separate library and be reused independandly. 
 - NOTE: Initially I was thinking to reuse something like Alamofire and Promise, however I thought that if I would be working in the company, I would want something to be able to control myself, something reusable, extendable and maintainable. This took a bit more time from me, but I think this is much better
 
### Repository ###
* Develop an API for `randomuser.me`. It will contan an entity, that implements `ZenlyEndpointDescription`, provides all the neccessary endpoints, url, paylod, endpoints, parameters etc it will be also a separate module and will also contain `Repository` class which is responsible in calling requests execution from network provider.
 
### UI Architecture ###
* MVP Based UI architecture for presenting and communicating with ViewState, Repository Component and presenting results in the UI

### Design ###
* Upgrade existing design via Sketch, please see below (not final, but will be looking better in the end of delivery)

### Image Loading and Scrolling Optimization ###
* Variant A: Right now commented, but we in the code you can see that the solution is using `DispatchQueue` and `dataTask` during `cellForItemAtIndexPath`.
* Variant B: This is an active solution, that is using collection view prefetching datasource and operations within operation queue.

![MY_COOL_IMAGE](https://bitbucket.org/aabrahamyan/zenly_photos/raw/7715f859846515ec053fba209887078c76d834a2/Design/photos.jpg)
